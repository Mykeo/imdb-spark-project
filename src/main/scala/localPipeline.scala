import imdbPipeline._
import imdbEtl._
import org.apache.spark.sql.{DataFrame, SaveMode}

/** Contains load methods.
 *
 * Take note that both object's methods overwrite data in the given location.
 */
object localPipeline {

  /** Saves DataFrame is the given location. Will overwrite data if not empty. */
  def saveCsv(frame: DataFrame, path: String): Unit = {
    frame.write.mode(SaveMode.Overwrite)
      .option("header", "true").csv(path)
  }

  /** Evokes every transformation method form imdbEtl and saves the output to a given location.  */
  def saveCsv(path: String): Unit = {
    saveCsv(ukrainianTitles(TitleAkas), path + "ukrainianTitles.csv")
    saveCsv(bornInThe19ThCentury(NameBasics), path + "bornInThe19ThCentury.csv")
    saveCsv(movies2HoursLong(TitleBasics), path + "movies2HoursLong.csv")
    saveCsv(actorsCharactersAndMovies(TitlePrincipals, NameBasics, TitleBasics), path + "actorsCharactersAndMovies.csv")
    saveCsv(adultMoviesToRegularMoviesPerRegion(TitleBasics, TitleAkas), path + "adultMoviesToRegularMoviesPerRegion.csv")
    saveCsv(highRatedTvSeries(TitleBasics, TitleEpisodes), path + "highRatedTvSeries.csv")
    saveCsv(bestTitlesByGenre(TitleBasics, TitleRatings), path + "bestTitlesByGenre.csv")
    saveCsv(bestTitlesByDecade(TitleBasics, TitleRatings), path + "bestTitlesByDecade.csv")
  }
}
