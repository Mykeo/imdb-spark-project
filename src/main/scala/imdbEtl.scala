import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

/** Provides transformation methods for the ETL.
 *
 * Each method's parameter name corresponds to a table name from IMDb dataset.
 */
object imdbEtl {

  /** Returns titles available in Ukraine. */
  def ukrainianTitles(titleAkas: DataFrame): DataFrame = {
    titleAkas.filter(col("region") === "UA")
      .select("title", "region")
  }

  /** Return list of people born in the 19th century. */
  def bornInThe19ThCentury(nameBasics: DataFrame): DataFrame = {
    nameBasics.filter(col("birthYear") <= lit("1900-01-01"))
      .filter(col("birthYear") >= lit("1801-01-01"))
      .select("primaryName", "birthYear")
  }

  /** Return movies that last more than 2 hours. */
  def movies2HoursLong(titleBasics: DataFrame): DataFrame = {
    val twoHours = 120

    titleBasics.filter(col("titleType") === "movie")
      .filter(col("runtimeMinutes") >= twoHours)
      .select("primaryTitle", "originalTitle", "titleType", "runtimeMinutes")
  }

  /** Return actors' names, characters they played and corresponding titles they contributed to. */
  def actorsCharactersAndMovies(titlePrincipals: DataFrame, nameBasics: DataFrame, titleBasics: DataFrame): DataFrame = {

    // Here we need to gather actors' name from nameBasics, film titles from titleBasics and actors themself from titlePrincipals.
    titlePrincipals.filter(col("characters").isNotNull)
      .join(nameBasics, "nconst")
      .join(titleBasics, "tconst")
      .select("primaryName", "primaryTitle", "originalTitle", "characters")
  }

  /** Return the ratio of adult movies to regular movies per region. */
  def adultMoviesToRegularMoviesPerRegion(titleBasics: DataFrame, titleAkas: DataFrame): DataFrame = {
    val topHundred = 100

    // Region data is kept in titleAkas, while whether the film is adult in titleBasics.
    titleBasics.join(titleAkas.withColumnRenamed("titleId", "tconst"), "tconst")
      .groupBy("region")
      .agg((sum("isAdult") / count("isAdult")).as("adultMoviePerRegularMovie"))
      .orderBy(col("adultMoviePerRegularMovie").desc)
      .select("region", "adultMoviePerRegularMovie")
      .limit(topHundred)
  }

  /** Returns top 50 most reviewed TV series. */
  def highRatedTvSeries(titleBasics: DataFrame, titleEpisode: DataFrame): DataFrame = {
    val topFifty = 50

    // TitleEpisode contains TV episodes and their parent TV Series, while titleBasics their names.
    val series = titleEpisode.groupBy("parentTconst")
      .agg(count("parentTconst").alias("totalEpisodes"))

    titleBasics.join(series.withColumnRenamed("parentTconst", "tconst"),"tconst")
      .orderBy(col("totalEpisodes").desc)
      .select("primaryTitle", "originalTitle", "titleType", "totalEpisodes")
      .limit(topFifty)
  }

  /** Returns top 10 most reviewed titles per genre. */
  def bestTitlesByGenre(titleBasics: DataFrame, titleRatings: DataFrame): DataFrame = {
    val topTen = 10

    val ratedTitles = titleBasics.join(titleRatings, "tconst")

    // Parse the genres.
    val genres = split(ratedTitles.col("genres"), ",")

    // Make sure each title has an only genre.
    val genreRatedTitles = ratedTitles.select(
      explode(genres).alias("genre"),
      col("primaryTitle"),
      col("originalTitle"),
      col("numVotes")
    )

    // Window functions allow to process each group individually.
    val marker = Window.partitionBy("genre")
      .orderBy(col("numVotes").desc)

    // First we need to get rid of TV episodes.
    genreRatedTitles.filter(col("titleType") =!= "tvEpisode")
      .withColumn("top", row_number().over(marker))
      .filter(col("top") <= topTen)
      .select("genre", "primaryTitle", "originalTitle", "numVotes")
  }

  /** Returns top 10 most reviewed titles per decade. */
  def bestTitlesByDecade(titleBasics: DataFrame, titleRatings: DataFrame): DataFrame = {

    val ratedTitles = titleBasics.join(titleRatings, "tconst")

    // Allows aggregation by decade and century.
    val pureYear = year(col("startYear"))
    val pureCentury = (pureYear - 1) / 100 + 1
    val pureDecade = (pureYear - 1) % 100 / 10 + 1

    // Integer type is preferred one.
    val titlesByDecade = ratedTitles.withColumn("century", pureCentury.cast(IntegerType))
      .withColumn("decade", pureDecade.cast(IntegerType))

    // Window utilities allow to assign row number for each value.
    val marker = Window.partitionBy("century", "decade")
      .orderBy(col("numVotes").desc)

    val top = 10

    // Partition does ordering in a group.
    titlesByDecade.filter(col("titleType") =!= "tvEpisode")
      .filter(col("startYear").isNotNull)
      .withColumn("top", row_number().over(marker))
      .filter(col("top") <= top)
      .select("century", "decade",  "primaryTitle", "originalTitle", "numVotes")
      .orderBy(col("century").asc, col("decade").asc, col("numVotes").desc)
  }
}
