import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.SparkFiles

/** Provides data for the ETL.
 *
 * The object contains schema definitions, URIs to IMDb API end-point as well as spark entry point and methods to access the data.
 *
 * Each name that resembles to IMDb table name is either a method that downloads a table from the remote storage (they start from capital letter),
 * path definition to API end-point (are in "URIs" object) or structure definitions for corresponding tables (are in "schemas" object).
 *
 * Take note that "crew" table found no use here.
 */
object imdbPipeline {

  /** Contains schemas definitions for every IMDb's table. */
  object schemas {
    val nameBasics: StructType = StructType(Seq(
      StructField("nconst", StringType),
      StructField("primaryName", StringType),
      StructField("birthYear", DateType),
      StructField("deathYear", DateType),
      StructField("primaryProfession", StringType),
      StructField("knownForTitles", StringType)
    ))

    val titleAkas: StructType = StructType(Seq(
      StructField("titleId", StringType),
      StructField("ordering", IntegerType),
      StructField("title", StringType),
      StructField("region", StringType),
      StructField("language", StringType),
      StructField("types", StringType),
      StructField("attributes", StringType),
      StructField("isOriginalTitle", IntegerType)
    ))

    val titleBasics: StructType = StructType(Seq(
      StructField("tconst", StringType),
      StructField("titleType", StringType),
      StructField("primaryTitle", StringType),
      StructField("originalTitle", StringType),
      StructField("isAdult", IntegerType),
      StructField("startYear", DateType),
      StructField("endYear", DateType),
      StructField("runtimeMinutes", IntegerType),
      StructField("genres", StringType)
    ))


    val titlePrincipals: StructType = StructType(Seq(
      StructField("tconst", StringType),
      StructField("ordering", StringType),
      StructField("nconst", StringType),
      StructField("category", StringType),
      StructField("job", StringType),
      StructField("characters", StringType)
    ))

    val titleEpisode: StructType = StructType(Seq(
      StructField("tconst", StringType),
      StructField("parentTconst", StringType),
      StructField("seasonNumber", IntegerType),
      StructField("episodeNumber", IntegerType)
    ))

    val titleRatings: StructType = StructType(Seq(
      StructField("tconst", StringType),
      StructField("averageRating", DoubleType),
      StructField("numVotes", IntegerType)
    ))
  }

  /** Contains IMDb API end-points here. */
  object URIs {
    private val baseUriPath: String = "https://datasets.imdbws.com/"

    val nameBasics: String = baseUriPath + "name.basics.tsv.gz"
    val titleAkas: String = baseUriPath + "title.akas.tsv.gz"
    val titleBasics: String = baseUriPath + "title.basics.tsv.gz"
    val titleEpisode: String = baseUriPath + "title.episode.tsv.gz"
    val titlePrincipals: String = baseUriPath + "title.principals.tsv.gz"
    val titleRatings: String = baseUriPath + "title.ratings.tsv.gz"
  }

  val spark: SparkSession = SparkSession.builder()
    .config("spark.master", "local[4]")
    .appName("imdb-pipeline")
    .getOrCreate()

  /** Makes possible to extract compressed data from IMDb dataset. */
  def loadTsv(tableName: String, tableSchema: StructType): DataFrame = {

    // Spark saves files to a temporary location.
    val tmpPath = SparkFiles.get(tableName)

    spark.read.option("compression", "gzip")
      .schema(tableSchema)
      .option("header", "true")
      .option("dataFormat", "yyyy")
      .option("nullValue", "\\N")
      .option("sep", "\t")
      .csv(tmpPath)
  }

  /** Returns DataFrame from remote storage. The method is an auxiliary function. */
  private def RetrieveTable(uri: String, schema: StructType): DataFrame = {
    spark.sparkContext.addFile(uri)
    loadTsv(uri.split("/").last, schema)
  }

  def NameBasics: DataFrame = RetrieveTable(URIs.nameBasics, schemas.nameBasics)
  def TitleBasics: DataFrame = RetrieveTable(URIs.titleBasics, schemas.titleBasics)
  def TitlePrincipals: DataFrame = RetrieveTable(URIs.titlePrincipals, schemas.titlePrincipals)
  def TitleEpisodes: DataFrame = RetrieveTable(URIs.titleEpisode, schemas.titleEpisode)
  def TitleRatings: DataFrame = RetrieveTable(URIs.titleRatings, schemas.titleRatings)
  def TitleAkas: DataFrame = RetrieveTable(URIs.titleAkas, schemas.titleAkas)
}
