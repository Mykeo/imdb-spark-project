
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

/** Contains test samples and expected ordered outputs.
 *
 * Contains mock data and expected sorted output, behaves like imdbEtl.
 *
 * Has spark entry point of its own.
 */
object mockPipeline {

  val spark: SparkSession = SparkSession.builder()
    .config("spark.master", "local[4]")
    .appName("mock-pipeline")
    .getOrCreate()

  // Case classes allow to enforce table schemas.
  private case class titleAkas(tconst: Option[String],
                  ordering: Option[Int],
                  title: Option[String],
                  region: Option[String],
                  language: Option[String],
                  types: Option[String],
                  attributes: Option[String],
                  isOriginalTitle: Option[Int])

  // Years will be converted to dates later.
  private case class titleBasics(tconst: Option[String],
                       titleType: Option[String],
                       primaryTitle: Option[String],
                       originalTitle: Option[String],
                       isAdult: Option[Int],
                       startYear: Option[String],
                       endYear: Option[String],
                       runtimeMinutes: Option[Int],
                       genres: Option[String])

  private case class titleEpisode(tconst: Option[String],
                          parentTconst: Option[String],
                          seasonNumber: Option[Int],
                          episodeNumber: Option[Int])

  private case class titlePrincipals(tconst: Option[String],
                             ordering: Option[Int],
                             nconst: Option[String],
                             category: Option[String],
                             job: Option[String],
                             characters: Option[String])

  private case class titleRatings(tconst: Option[String],
                          averageRating: Option[Double],
                          numVotes: Option[Int])

  private case class nameBasics(nconst: Option[String],
                        primaryName: Option[String],
                        birthYear: Option[String],
                        deathYear: Option[String],
                        primaryProfession: Option[String],
                        knownForTitles: Option[String])

  import spark.implicits._

  // Case classes require Option type objects. The functions below convert them automatically to needed types.
  implicit def stringToOption(line: String): Option[String] = Option(line)
  implicit def intToOption(value: Int): Option[Int] = Option(value)
  implicit def doubleToOption(value: Double): Option[Double] = Option(value)

  val TitleBasics: DataFrame = {
    Seq(titleBasics("tt0439572", "movie", "The Flash", "The Flash", 1, "2022", None, None, "Action,Adventure"),
      titleBasics("tt0490215", "movie", "Silence", "Silence", 0, "2016", None, 161, "Drama,History"),
      titleBasics("tt0475784", "tvSeries", "Westworld", "Westworld", 0, "2016", None, 62, "Drama,Mystery,Sci-Fi"),
      titleBasics("tt0489974", "tvSeries", "Carnival Row", "Carnival Row", 0, "2019", None, 56, "Crime,Drama,Fantasy"),
      titleBasics("tt4227538", "tvEpisode", "The Original", "The Original", 0, "2016", None, 68, "Drama,Mystery,Sci-Fi"),
      titleBasics("tt4562758", "tvEpisode", "Chestnut", "Chestnut", 0, "2016", None, 58, "Drama,Mystery,Sci-Fi"),
      titleBasics("tt4625856", "tvEpisode", "The Stray", "The Stray", 0, "2016", None, 59, "Drama,Mystery,Sci-Fi"),
      titleBasics("tt4363218", "tvEpisode", "Some Dark God Wakes", "Some Dark God Wakes", 0, "2019", None, 60, "Crime,Drama,Fantasy"),
      titleBasics("tt4391820", "tvEpisode", "Aisling", "Aisling", 0, "2019", None, 51, "Crime,Drama,Fantasy"))
      .toDF()
      .withColumn("datedStartYear", to_date(col("startYear"), "yyyy"))
      .withColumn("datedEndYear", to_date(col("endYear"), "yyyy"))
      .drop("startYear")
      .drop("endYear")
      .withColumnRenamed("datedStartYear", "startYear")
      .withColumnRenamed("datedEndYear", "endYear")
  }

  val TitleEpisodes: DataFrame = {
    Seq(titleEpisode("tt4227538", "tt0475784", 1, 1),
      titleEpisode("tt4562758", "tt0475784", 1, 2),
      titleEpisode("tt4625856", "tt0475784", 1, 3),
      titleEpisode("tt4363218", "tt0489974", 1, 1),
      titleEpisode("tt4391820", "tt0489974", 1, 2))
      .toDF()
  }

  val TitlePrincipals: DataFrame = {
    Seq(titlePrincipals("tt0489974", 1, "nm0089217", "actor", null, "Rycroft"),
      titlePrincipals("tt0489974", 2, "nm5353321", "actress", null, "Vignette"),
      titlePrincipals("tt0489974", 3, "nm2012438", "writer", "creator", null),
      titlePrincipals("tt0475784", 1, "nm0939697", "actress", null, "Dolores"),
      titlePrincipals("tt0490215", 1, "nm1940449", "actor", null, "Rodrigues"),
      titlePrincipals("tt0439572", 1, "nm0000255", "actor", null, "Batman"),
      titlePrincipals("tt0439572", 2, "nm1953833", "producer", "producer", null))
      .toDF()
  }

  val TitleRatings: DataFrame = {
    Seq(titleRatings("tt0490215", 7.2, 99536),
      titleRatings("tt0475784", 8.7, 422359),
      titleRatings("tt0489974", 7.9, 44597),
      titleRatings("tt4227538", 8.9, 20903),
      titleRatings("tt4562758", 8.5, 15800),
      titleRatings("tt4625856", 8.4, 14431),
      titleRatings("tt4363218", 7.4, 2172),
      titleRatings("tt4391820", 7.5, 1789))
      .toDF()
  }

  val TitleAkas: DataFrame = {
    Seq(titleAkas("tt0490215", 1, "Silence", "US", "en", null, null, 0),
      titleAkas("tt0490215", 2, "Молчание", "RU", null, "imdbDisplay", null, 0),
      titleAkas("tt0490215", 2, "Мовчання", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0489974", 1, "Carnival Row", "ES", null, "imdbDisplay", null, 0),
      titleAkas("tt0489974", 2, "Карнівал Роу", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0439572", 1, "The Flash", "FR", null, "imdbDisplay", null, 0),
      titleAkas("tt0439572", 2, "Флеш", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0475784", 1, "Світ «Дикий Захід»", "UA", null, "imdbDisplay", null, 0))
      .toDF()
  }

  val NameBasics: DataFrame = {
    Seq(nameBasics("nm0089217", "Orlando Bloom", "1977", null, "actor,producer", "tt0325980"),
      nameBasics("nm5353321", "Cara Delevingne", "1992", null, "actress", "tt1781769"),
      nameBasics("nm2012438", "Travis Beacham", null, null, "writer,producer", "tt0489974"),
      nameBasics("nm0939697", "Evan Rachel Wood", "1987", null, "actress", "tt0445922"),
      nameBasics("nm1940449", "Andrew Garfield", "1983", null, "actor,producer", "tt1872181"),
      nameBasics("nm0000255", "Ben Affleck", "1972", null, "producer,actor", "tt0119217"),
      nameBasics("nm1953833", "Barbara Muschietti", null, null, "producer,writer", "tt0116250"))
      .toDF()
      .withColumn("datedBirthYear", to_date(col("birthYear"), "yyyy"))
      .withColumn("datedDeathYear", to_date(col("deathYear"), "yyyy"))
      .drop("birthYear")
      .drop("deathYear")
      .withColumnRenamed("datedBirthYear", "birthYear")
      .withColumnRenamed("datedDeathYear", "deathYear")
  }

  // Sample output tables. They are sorted intentionally.
  val titlesAvalilableInUkraine: DataFrame = {
    Seq(titleAkas("tt0490215", 2, "Мовчання", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0490215", 2, "Карнівал Роу", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0475784", 2, "Флеш", "UA", null, "imdbDisplay", null, 0),
      titleAkas("tt0475784", 1, "Світ «Дикий Захід»", "UA", null, "imdbDisplay", null, 0))
      .toDF()
      .select("title", "region")
      .orderBy(col("title"), col("region"))
  }

  // No need to order. The frame is empty.
  val peopleBornInThe19ThCentury: DataFrame = {
    spark.emptyDataset[nameBasics].select("primaryName", "birthYear")
  }

  // No need to order. There is an only value.
  val moviesThatLastLongerThan2Hours: DataFrame = {
    Seq(titleBasics("tt0490215", "movie", "Silence", "Silence", 0, "2016", None, 161, "Drama,History"))
      .toDF()
      .select("primaryTitle", "originalTitle", "titleType", "runtimeMinutes")
  }

  val actorsAndTheirCharactersWithMovies: DataFrame = {
    Seq[(String, String, String, String)](
      ("Orlando Bloom", "Carnival Row", "Carnival Row", "Rycroft"),
      ("Cara Delevingne", "Carnival Row", "Carnival Row", "Vignette"),
      ("Evan Rachel Wood", "Westworld", "Westworld", "Dolores"),
      ("Andrew Garfield", "Silence", "Silence", "Rodrigues"),
      ("Ben Affleck", "The Flash", "The Flash", "Batman"))
      .toDF("primaryName", "primaryTitle", "originalTitle", "characters")
      .orderBy(col("primaryName"), col("primaryTitle"), col("originalTitle"), col("characters"))
  }

  val adultMoviesPerRegion: DataFrame = {
    Seq[(String, Double)](
      ("US", 0.0), ("RU", 0), ("UA", 0.25), ("ES", 0.0), ("FR", 1.0))
      .toDF("region", "adultMoviePerRegularMovie")
      .orderBy(col("region"), col("adultMoviePerRegularMovie").desc)
  }

  val top50LongestTvSeries: DataFrame = {
    Seq[(String, String, String, Int)](
      ("Westworld", "Westworld", "tvSeries", 3),
      ("Carnival Row", "Carnival Row", "tvSeries", 2))
      .toDF("primaryTitle", "originalTitle", "titleType", "completeSeries")
      .orderBy(col("primaryTitle"), col("originalTitle"), col("titleType"), col("completeSeries").desc)
  }

  // The Flash is to be released in the future, that's why it has no rating yet.
  val top10TitlesPerDecade: DataFrame = {
    Seq[(Int, Int, String, String, Int)](
      (21, 2, "Westworld", "Westworld", 422359),
      (21, 2, "Silence", "Silence", 99536),
      (21, 2, "Carnival Row", "Carnival Row", 44597))
      .toDF("century", "decade", "primaryTitle", "originalTitle", "numVotes")
      .orderBy(col("century"), col("decade"), col("primaryTitle"), col("originalTitle"),
        col("numVotes").desc)
  }

  val top10TitlesPerGenre: DataFrame = {
    Seq[(String, String, String, Int)](
      ("Drama", "Westworld", "Westworld", 422359),
      ("Drama", "Silence", "Silence", 99536),
      ("Drama", "Carnival Row", "Carnival Row", 44597),
      ("Mystery", "Westworld", "Westworld", 422359),
      ("Sci-Fi", "Westworld", "Westworld", 422359),
      ("History", "Silence", "Silence", 99536),
      ("Crime", "Carnival Row", "Carnival Row", 44597),
      ("Fantasy", "Carnival Row", "Carnival Row", 44597))
      .toDF("genre", "primaryTitle", "originalTitle", "numVotes")
      .orderBy(col("genre"), col("primaryTitle"), col("originalTitle"), col("numVotes").desc)
  }
}
