import org.scalatest.flatspec.AnyFlatSpec

import mockPipeline._
import imdbEtl._

/** Runs test for each transformation method.
 *
 * Every test must return same output, like in mockPipeline.
 */
class testEtl extends AnyFlatSpec {
  "ukrainianTitles" must "be empty" in {
    assert(ukrainianTitles(TitleAkas).except(titlesAvalilableInUkraine).count() == 0)
  }
  "bornInThe19ThCentury" must "be empty" in {
    assert(bornInThe19ThCentury(NameBasics).except(peopleBornInThe19ThCentury).count() == 0)
  }
  "moviesThatLastLongerThan2Hours" must "be empty" in {
    assert(movies2HoursLong(TitleBasics).except(moviesThatLastLongerThan2Hours).count() == 0)
  }
  "actorsCharactersAndMovies" must "be empty" in {
    assert(actorsCharactersAndMovies(TitlePrincipals, NameBasics, TitleBasics).except(actorsAndTheirCharactersWithMovies).count() == 0)
  }
  "adultMoviesToRegularMoviesPerRegion" must "be empty" in {
    assert(adultMoviesToRegularMoviesPerRegion(TitleBasics, TitleAkas).except(adultMoviesPerRegion).count() == 0)
  }
  "highRatedTvSeries" must "be empty" in {
    assert(highRatedTvSeries(TitleBasics, TitleEpisodes).except(top50LongestTvSeries).count() == 0)
  }
  "bestTitlesByGenre" must "be empty" in {
    assert(bestTitlesByGenre(TitleBasics, TitleRatings).except(top10TitlesPerGenre).count() == 0)
  }
  "bestTitlesByDecade" must "be empty" in {
    assert(bestTitlesByDecade(TitleBasics, TitleRatings).except(top10TitlesPerDecade).count() == 0)
  }
}
