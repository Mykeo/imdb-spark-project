# IMDb Spark Project

Develop your own first ETL pipeline to process [IMDb](https://www.imdb.com/interfaces/) dataset using Apache Spark and Scala language.

## Problem statemens

- ### Extraction

	- Create appropriate schemas for all datasets is use.
	- Depending on the schemas above create corresponding DataFrames by reading data from the remote storage.
	- Check whether everything went right.
 
- ### Transformation

	- Get all titles of piece of art that are available in Ukrainian.
	- Get the list of people’s names, who were born in the 19th century.
	- Get titles of all movies that last more than 2 hours.
	- Get names of people, corresponding movies/series and characters they played in those films.
	- Get the top 100 regions with highest relation "adult movies to regular movies".
	- Get the top 50 TV Series with the biggest quantity of episodes.
	- Get 10 titles of the most popular product of art by each decade. 
	- Get 10 titles of the most popular product of art by each genre.
	
- ### Loading

	- Load all results of transformations to .csv files.


## Implementation

The solution contains such classes:
1. imdbEtl - does data transformations.
2. imdbPipeline - loads data from the remote storage.
3. localPipeline - saves output into 8 folders of .csv format.
4. mockPipeline - contains mock data for testing.
5. testEtl - tests imdbEtl functionality.

Also, you may want to check build.sbt contents.

## Note

The solution is no stand-alone application, it's a package. Look up localPipeline's saveCsv method description for more information.

